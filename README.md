# eldiego

Ahora también sos un programa Diego!

Código Arduino que visualiza imágenes de *El Diego* en un display oled de 124x64.
Las imágenes estan harcodeadas en hexa dentro del mismo código.

## Instrucciones

- Conectá tu OLED I2C a un Arduino 
- Pasale el programa ``eldiego.ino``
- Reproducí Live is Life, https://www.youtube.com/watch?v=s7ZjU-6iSwk
- Recordá al inmenso ídolo mirando el display y andá a secarte esas lágrimas me hacés el favor!

## Licencia 

GPLv3